function save() {
    var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var data = Blockly.Xml.domToText(xml);
  
    // Store data in blob.
    // var builder = new BlobBuilder();
    // builder.append(data);
    // saveAs(builder.getBlob('text/plain;charset=utf-8'), 'blockduino.xml');
    console.log("saving blob");
    var blob = new Blob([data], {type: 'text/xml'});
    saveAs(blob, 'blocklyProject.xml');
  }
  /**
   * Load blocks from local file.
   */
   function load(event) {
    var files = event.target.files;
    // Only allow uploading one file.
    if (files.length != 1) {
      return;
    }
  
    // FileReader
    var reader = new FileReader();
    reader.onloadend = function(event) {
      var target = event.target;
      // 2 == FileReader.DONE
      if (target.readyState == 2) {
        loadTxtToBlockly(target.result);
      }
      // Reset value of input after loading because Chrome will not fire
      // a 'change' event if the same file is loaded again.
      document.getElementById('loadFile').value = '';
    };
    reader.readAsText(files[0]);
  }
  
  function loadTxtToBlockly(text)
  {
      try {
          var xml = Blockly.Xml.textToDom(text);
        } catch (e) {
          alert('Error leyendo el archivo. Probablemente esté dañado:\n' + e);
          return;
        }
        var count = Blockly.mainWorkspace.getAllBlocks().length;
        if (count && confirm('¿Reemplazar el programa actual con el programa cargado??\n"Cancelar" mezclará el programa cargado con el existente.')) {
          Blockly.mainWorkspace.clear();
        }
        Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
  }
  